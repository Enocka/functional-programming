object random {
  // aMethod(1) returns ()
  def aMethod(a: Int) { true }

  // aFunction(1) returns true
  val aFunction = (a: Int) => true
}