object countingChange {

  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0 || coins.isEmpty) 0;
    else if (money < 0) 0;
    else if (money == coins.head) 1 + countChange(money, coins.tail)

    else countChange(money - coins.head, coins) + countChange(money, coins.tail)
  }
  countChange(100, List(100, 50, 40, 20, 10, 5, 1));
}
