object paranthesesBalancing {

  def balance(chars: List[Char]): Boolean = {

    def balancer(passedString: List[Char], matching: Int) :Boolean = {
      if (passedString.isEmpty) matching == 0;

      else if (passedString.head == List('(').head) balancer(passedString.tail, matching-1);
      else if (passedString.head == List(')').head && !(matching < 0)) false;
      else if (passedString.head == List(')').head) balancer(passedString.tail, matching+1);

      else balancer(passedString.tail, matching);
    }
    balancer(chars, 0);

  }
  balance(("(if (zero? x) max (/ 1 x))").toList);
  balance(("I told him (that it’s not (yet) done). (But he wasn’t listening)").toList);
  balance(("())(").toList);
  balance((":-)").toList);
}